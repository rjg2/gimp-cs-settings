# GIMP 2.8 + CS Settings #

My personal settings of GIMP. The aim with these settings is to make it easier to transition from CS.

This script will download/install the latest version of gimp along with some extra plugins and apply settings.

More to come...



```
#!bin/bash

git clone https://rjg2@bitbucket.org/rjg2/gimp-cs-settings.git
```



Ubuntu/Deb


```
#!bin/bash

cd gimp-cs-settings
chmod +x install_gimp.sh
./install_gimp.sh

```