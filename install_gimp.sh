#!/bin/bash

TMP=/tmp

# add repos
sudo apt-add-repository -y ppa:otto-kesselgulasch/gimp

# basic update
sudo apt-get -y --force-yes update

# install apps
sudo apt-get -y install \
    gimp gimp-gmic gimp-gutenprint gimp-plugin-registry \
    wget tar gzip

# backup existing gimp settings
if [ -d ~/.gimp-2.8 ]; then
	cp -r ~/.gimp-2.8 ~/.gimp-2.8-$(date +%Y%m%d)
    echo "Backed up existing gimp settings: ~/.gimp-2.8-$(date +%Y%m%d)"
fi

# copy settings
cp -r .gimp-2.8 ~/

# install resynthesizer plugin
if [ -f $TMP/resynthesizer.tar_0.gz ]; then
	sudo rm $TMP/resynthesizer.tar_0.gz
fi
wget -P $TMP http://registry.gimp.org/files/resynthesizer.tar_0.gz
gzip -d $TMP/resynthesizer.tar_0.gz
mv $TMP/resynthesizer.tar_0 ~/.gimp-2.8/plug-ins/resynthesizer.tar

# install heal selection
if [ -f $TMP/smart-remove.scm ]; then
	sudo rm $TMP/smart-remove.scm
fi
wget -P $TMP http://registry.gimp.org/files/smart-remove.scm
mv $TMP/smart-remove.scm ~/.gimp-2.8/scripts

# install ak lab sharpen
wget -P $TMP http://registry.gimp.org/files/AK-LABSharpen.scm
mv $TMP/AK-LABSharpen.scm ~/.gimp-2.8/scripts

echo "All done!"