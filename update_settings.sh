#!/bin/bash

# backup existing gimp settings
if [ -d ~/.gimp-2.8 ]; then
	cp -r ~/.gimp-2.8 ~/.gimp-2.8-$(date +%Y%m%d)
    echo "Backed up existing gimp settings: ~/.gimp-2.8-$(date +%Y%m%d)"
fi

# copy settings
cp -r .gimp-2.8 ~/

echo "All done, settings up to date!"